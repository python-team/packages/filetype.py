#/bin/bash

#cat ../README.rst | grep "Supported types" -A 2000 | grep -v Supported | \
#    egrep -v '^(\.\.| )' | cut -d"-" -f2 | tr -d '[*^]' | xargs | \
#    sed s'/ /, /g' | \
#    sed 's/Image,/Image:/' | \
#    sed 's/, Video,/\n\nVideo:/' | \
#    sed 's/, Audio,/\n\nAudio:/' | \
#    sed 's/, Archive,/\n\nArchive:/' | \
#    sed 's/, Font,/\n\nFont:/' | \
#    sed 's/$/./'

cat ../README.rst | grep "Supported types" -A 2000 | grep -v Supported | \
    egrep -v '^(\.\.| )' | cut -d"-" -f2 | tr -d '[*^]' | xargs | \
    sed 's/Image/Image:/' | \
    sed 's/ Video/\nVideo:/' | \
    sed 's/ Audio/\nAudio:/' | \
    sed 's/ Archive/\nArchive:/' | \
    sed 's/ Font/\nFont:/' > types.tmpfile

for i in Image Video Audio Archive Font
do
    printf "   - $i: "
    cat types.tmpfile | grep $i | cut -d: -f2 | tr ' ' '\n' | sort -n | \
        xargs | sed 's/ /, /g' | sed 's/$/./' | fold -sw 69
done
